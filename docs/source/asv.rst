Amplicon Sequence Variants (ASVs)
=================================



**Define ASVs and their abundance**

Move to data folder::

      cd /emiprat/data/amplicons

Move _R1.fastq file to R1 folder and _R2.fastq to folder R2::

     cp * .R1.fastq /emiprat/amplicons/asv/R1
     cp * .R2.fastq /emiprat/amplicons/asv/R2


Move to the dada2 directory and lunch the R script::

     cd /emiprat/amplicons/asv
     Rscript  ./dada.script/dada2_180719_big.data_wd.R


