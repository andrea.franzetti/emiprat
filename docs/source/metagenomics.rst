Metagenomics
============

**Setting variables**

In the file "conf_cluster.conf" you can set environment variables and bioinformatic parameters for the analysis. 

Open the file with an editor::

	vim conf_cluster.conf 

and modify it following the instructions of the instructor.

To make the changes active source the file::

	source conf_cluster.conf 

Contents:

.. toctree::
   :maxdepth: 2

   trimming
   assembly
   binning
   prodigal
   annotation
   mapping
   parsing
   mining

