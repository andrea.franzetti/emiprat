Data analysis
==================

Create table and graphical outputs::

	source conf_cluster.conf
        cd $OUT
        sed -i 's/ /_/g' join_gene_id_seed.tab.pivot


Create a taxonomy Krona graph of all the community::

	t=6
	for i in mock1 mock2 mock3; do
		ktImportTaxonomy -o $i.all.taxonomy.krona.html -t 1 -m $t join_gene_id_seed.tab.pivot
		t=$((t + 1))
	done

Create a SEDD Krona graph of all the community::

	s=6
	for i in mock1 mock2 mock3; do
		paste <(cut -f$s join_gene_id_seed.tab.pivot) <(cut -f2,3,4,5 join_gene_id_seed.tab.pivot) > $i.all.seed.tab
		ktImportText $i.all.seed.tab -o $i.all.seed.html
		s=$((s + 1))
	done


Create sub-table based on a SEED category::

	python $wd/mining.py join_gene_id_seed.tab.pivot lev1 "Photosynthesis"
	
Create taxonomy Krona graphs based on the sub-table::

	m=6
	for i in mock1 mock2 mock3; do

		ktImportTaxonomy -o $i.Photosynthesis.taxonomy.krona.html -t 1 -m $m Photosynthesis.seed_tax.tab
		m=$((m + 1))

	done


