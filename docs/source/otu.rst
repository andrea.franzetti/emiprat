Operational Taxonomic Units (OTUs)
==================================

**Define OTUs and their abundance**

Move to data folder::

      cd /emiprat/data/amplicons


Copy fastq files and move to otu folder::

     cp *fastq /emiprat/amplicons/otu/
     cd /emiprat/amplicons/otu/

Lunch the script and answer to the questions (use name_list file)::

     ./Illumina.sh


