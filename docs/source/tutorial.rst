Bash Tutorial
==============


**Introduction to the BASH shell and the FASTQ format**


**Rationale**

Most bioinformatics work is done through a **command-line interface**, which rather than the graphical interfaces that we use every day needs exact text commands.
It can be a little **intimidating** at first, but once you understand how it works it will hopefully be clear to you why it is done this way.
The command line allows you to interact with your computer through an entirely text-based interface, allowing you to easily perform repeated operations on different datasets or to perform these analyses on a cluster of computers or on a remote machine for large scale tasks.  
:scream: **Don't be scared!!! We will start with basic concepts** :smirk:

Every unix-based system (**it is hard to say but also Mac are UNIX-based systems**) has a **Terminal** and it usually uses a command-line system called **bash**.

.. image:: Unix_history-simple.svg

*https://it.wikipedia.org/wiki/Unix*

**(If you are Linux or Mac users and do you want to repeat the tutorial you can find the Terminal under Applications/Utilities.)**

**Some infos about Operative System**

The Operative System (OS) id the environment in which the programs are executed. Consequently it allows the access to computational and storage resources.
It is possible to define some common characteristics of OSs:
- User interface: today almost all the OS have two interfaces
    - *Command line interface* (**CLI**) <-- ***This is exactly what we are going to talk about***
    - *Graphic User Interface* (**GUI**)
- Programs execution;
- File system management;
- Communication between processes;
- Tracking of errors.

The **CLI** allows you to give commands directly to the OS. Its function is to translate the user choice in the machine language. It is exactly what happens when you use the *Google Translator*.   
We are going to use the **bash** shell, but just to say there are others **terminal shell**, such as **csh** or **ksh**.    
Most of the bash commands used in the Terminal can also be used on Linux or in a Linux emulator, like for example Cygwin, running under Windows.

All bash programs shows a similar usage.  

|    Program    | Options (Flags) | Object |
|---------------|-----------------|--------|
| Program name  | Options modify the program output|the object on which the program is executed|

Below are some exercises intended to help you understand how bash commands work, and what the advantages are.

**Bash tips**
You are now located in your home directory (denoted *~*, **Please keep in mind this symbol**). [Every users has his own **home**.]   
You can always type::
    `pwd`
(present working directory) to locate yourself in your file system. 
The **pwd** command gives you the **path** of your current position in the system.

.. image:: folders.png


Type::

    [User_name ~]: pwd
    /home/User_name

The **path** is the description of where something is located in the file system and is a list of directory names separated by slashes.
In Mac, a convenient way to enter long paths in the terminal window is to just drag and drop the file or folder from the finder into a terminal window.
It can also be a good idea to have a **Finder** (or more generally the file manager available in your system) window open next to the Terminal.  

**Shortcuts**: While typing in the Terminal, there are some shortcuts that will make your life easier.
For example, the **UP arrow** will bring back the last command, 

and the **TAB**  button will auto-complete a command or filename (as long as it is unambiguous from what you have already typed).

**By typing `ls` (list) you can see all the contents of the folder you are in.**::

    [User_name ~]: ls

All bash commands have a manual page. By typing **man [command]** you can view it. Within the manual, the space bar will move you down a page and **q** will quit.

**Now, open the manual page for the ls command.**::

    [User_name ~]: man ls
  
**Exit the manual page my typing 'q'.**   


By adding “*flags*” after a command (usually with space, then a dash **-** and a letter code), we can alter the execution of the command.  
For example, the flag **–l**, when added to the ls command, prints the contents of a folder in long format. Try this::

    [User_name ~]: ls -l
    total 0
    drwxr-xr-x    3 User_name  User_name     96 Mar 30 11:27 Documents/

The **ls -l** command prints out permissions for each file in a folder, like this:
drwxr-xr-x, where the *d*, if present, means that the item is a directory and r=read, w=write, x=execute (for programs).  
The permissions are listed in the following order: *User*, *Group* and *All*.  
It means the User (who created the folder) can read, write and execute, while Group and All may only read and execute.  

In Unix system to make accessible files and programs to different users, the **System Administrator** may create **Groups**. In this way it is possible to facilitate data usage and optimize the storage.



To create new folders and to copy or move files we usually use Finder on a Mac, but this can be done also with the Terminal.
To create a new folder the command is **mkdir** (make directory).  In your "home" create a folder named "bash_tutorial"::

    [User_name ~]: mkdir bash_tutorial
    [User_name ~]: ls

The **cd** (change directory) program is used to move among directories::

    [User_name ~]: cd bash_tutorial
    [User_name ~]: pwd
    /home/User_name/bash_tutorial

If you get lost, you can always return to your home directory with **cd**::

    [User_name ~]: cd
    [User_name ~]: pwd
    /home/User_name/
    
Just to make it complete clear **cd** without any object move you to the *user home folder*.  

There are other *shortcuts* you need to keep in mind:  
    - `..` means previous folder;  
    - `.`  current previous folder;
So for example if you would like to list the content of the /home/ folder in our example you may just type::

    [User_name ~]: ls ..
  
Now, we copy the files which we will use for more advanced bash commands, from a shared folder to your new *bash_tutorial* folder. The files are located in */media/practical*. 
 
Apen the browser and open the Google Drive folder https://drive.google.com/drive/folders/14qgCjHMgeCQA94ICUvE74FI0F9jE4pnA?usp=sharing and dowload the file bash_tutorial_R1.fq


Move to the HOME folder and copy the file into the folder /media/bash_tutorial::

    cd $HOME
    ls
    cp bash_tutorial_R1.fq $HOME/bash_tutorial

TIP: you can use * as a wildcard to choose all files with a certain suffix, i.e. “*.fastq” or “*.fq”::

    cp *fq $HOME/bash_tutorial



**Now, move back to your “bash_tutorial” folder.**

Remember, by typing **ls** (list) you can see all the contents of the folder you are in.

All bash commands have a manual page. By typing man [command] you can view it. Within the manual, the space bar will move you down a page and q will quit.


---------------------------------------------------------------------------------------------------------------------
### FASTQ format
FASTQ (https://en.wikipedia.org/wiki/FASTQ_format) files are text files containing sequence data with a quality (Phred) score for each base, represented as an ASCII character.   
The quality score is an integer (Q) which is typically in the range 2 - 40, but higher and lower values are sometimes used. In particular, versions 1.8 and later of the Illumina platform generate reads with Q scores up to 41.  
  
*FASTQ read with 50nt in Illumina format (ASCII_BASE=33).*  
There are always four lines per read:
1. The first line starts with '@', followed by the label (read identifier);  
2. The second line contains the observed sequence;
3. The third line starts with '+'. In some variants, the '+' line contains a second copy of the label;
4. The fourth line contains the Q scores represented as ASCII characters.
---------------------------------------------------------------------------------------------------------------------

To view what the fastq files look like, the edit command would open a file in a Text Editor, such as **gedit** (**NOT WORD OFFICE or any similar software!!!**).  
With the abbreviated test files, it might work, but normal fastq files are too large.  
We can, however, open a part of the file using the editor **less**.   

.. image:: fastq_fig.jpg


**Use `less` to view a fastq file**. You can move down through the file with space, and quit back to the Terminal with *q*::
    
    less bash_tutorial_R1.fq
    

You can also view the top or bottom 10 lines of a file with the commands *head` and tail*.

**Use tail to view the bottom 10 lines of a fastq file.**::

    tail bash_tutorial_R1.fq

**Now, use the manual page for `tail` to find out how to view more than 10 lines and use `tail` again to view the bottom 50 lines of a fastq file.**::

    man tail
    tail -n 50 bash_tutorial_R1.fq


This prints out the lines to the screen.  
However, we can redirect the output to a file with the *>* symbol. Like this::
    
    [print command] > file.txt  

The double angle bracket, *>>*, does the same thing but appends information to a file rather than overwriting it.

**Use the > to print the last 50 lines of a fastq file to a new file, called tail50.txt
Open the new file in less with less tail50.txt**::

    tail -n 50 bash_tutorial_R1.fq > tail50.txt
    less tail50_R1.txt

Quit *less*  

One of the most useful commands in the Terminal is *grep*. This command searches through a text for lines matching a given argument and prints out only the lines that contain the argument.  
Try using grep to pull out any read that contains the sequence **CTCCTAC** from a fastq file::

    grep CTCCTAC bash_tutorial_R1.fq

Used in combination with other commands, grep can do almost anything. To combine commands, we can use the “pipe”, *|*. It takes the output from one command and feeds it into another command.

The command *cat* prints a whole text file or combines several files into one and prints to the screen. Using the *–b* flag, you can also count lines::

    cat -b tail50_R1.txt

Now an example on it's possible combine more commands.:

    tail -n 50 bash_tutorial_R1.fq | grep '^@'
    
    

This will print only the lines starting with *@* symbol, i.e. the identifier lines, out of the last 50 lines of a file. The *ˆ* means "*start with*".  
Use *grep -c*  to find out how many reads there are in one of the fastq files::

    grep -c '^@' bash_tutorial_R1.fq

Check the number of lines. 

**Use grep to pull out all the identifier lines from samples `bash_tutorial_R1.fq` and redirect them into a new file called `identifiers_R1.txt`**::

    grep '^@' bash_tutorial_R1.fq > identifiers_R1.txt



*grep* allows you to search in text file by using the *Regular Expressions*.  

**What are regular expressions?**  
Regular expressions are special sequences of characters that define a possibly complex search pattern.
For example:
 - `^` matches the beginning of the line;
 - `.` matches any character
 - {m,n}   matches the preceding elements at least m but not more than n times.

The Regexp pattern language is unlike most other computing languages. It can be best learned via an interactive service like https://regexone.com/ or many others where the pattern and its effect are instantly visualized.  

*how to convert a fastq file into a fasta in bash*::

    grep -A1 '^@M00' bash_tutorial_R1.fq > example.fa
    sed -i -e 's/^@/>/g' example.fa
    sed -i -e 's/^--//g' example.fa
    sed -i -e '/^$/d' example.fa


**Execise:** Create a .fastq file starting from "bash_tutorial_R1.fq" with all the reads that begin with 'CGCG' string.
