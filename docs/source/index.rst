Environmental Microbiology Practical Training (EMIPRAT) - University of Milano Bicocca
======================================================================================

Welcome to the practical at **Environmental Microbiology Practical Training (EMIPRAT)**

This practical aims at running a complete bioinformatic analysis for both amplicon sequence data of 16S rRNA and a metagenomics dataset.

The practical is partially based of the software repository https://gitlab.com/andrea.franzetti/emiprat, which contains all the third-part software and dependencies to run a complete bioinformatic analysis of a metagenomics dataset. 
The following programs are used for the analysis:

* Usearch (https://www.drive5.com/usearch/)
* Dada2 (https://benjjneb.github.io/dada2/index.html)
* Quality Filtering: Sickle (https://github.com/najoshi/sickle)
* Assembly: IDBA-UD (https://github.com/loneknightpy/idba)
* Gene prediction: Prodigal (https://github.com/hyattpd/Prodigal)
* Annotation: DIAMOND (https://github.com/bbuchfink/diamond)
* Alignment: Bowtie2 (http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
* Alignment file manipulation: Samtools, Bedtools (http://samtools.sourceforge.net/; http://bedtools.readthedocs.io/en/latest/)
* Taxonomic assignemts and parsing: MEGAN, Krona, in-house scritps (http://ab.inf.uni-tuebingen.de/software/megan6/; https://github.com/marbl/Krona/wiki)
* Graphical output: Krona


Contents:

.. toctree::
   :maxdepth: 2
   
   tutorial
   ssu
   metagenomics

