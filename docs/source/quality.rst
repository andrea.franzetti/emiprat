Quality control
===============

Source the configuration file::

	source conf_cluster.conf

FastQC provides a report of the quality of your Illumina sequences. First, create a directory to save the results of the quality check::

	mkdir -p $wd/$PRJ_NAME/0_QualityControl

Load fastqc module::

	module load profile/advanced
	module load profile/bioinf
	module load autoload fastqc

Run FastQC::

        for i in $SAMPLE_LIST; do fastqc -o $wd/$PRJ_NAME/0_QualityControl --nogroup $INPUT_DIR/$i.R1.fastq $INPUT_DIR/$i.R2.fastq; done

In a shell of YOUR COMPUTER, create a directory and copy the FastQC output::

	mkdir -p ~/icme9/fqc-results
	cd ~/icme9/fqc-results
	scp -r userneme@login.marconi.cineca:/path/to/quality_control/*html
