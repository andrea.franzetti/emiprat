Parsing annotation
==================

**Use MEGAN tools to parse the outputs**::

	cd $wd
	source conf_cluster.conf

Open MEGAN software::

	MEGAN

**Follow instructions to "meganize" the blast output**

Extract functional information from the rma file::

	./megan/tools/rma2info -i $GENE_PRED/diamond.prodigal.idba.practical.rma6 -u false -p true -r2c SEED -n true -v > $GENE_PRED/id_gene2seed.$PRJ_NAME.tab


Open $GENE_PRED/id_gene2seed.$PRJ_NAME.tab with GEDIT; find "'\'t"and replace with ";" find ";'\n'" with "'\n'"


Extract functional taxonomic from the rma file::
	./megan/tools/rma2info -i $GENE_PRED/diamond.prodigal.idba.practical.rma6 -u false -r2c Taxonomy -v > $GENE_PRED/id_gene2tax.$PRJ_NAME.tab


**Merge all the outputs in a single table**

Create output directory::

	mkdir -p $OUT

Move to coverage folder and create variables for next steps::

	cd $wd/$PRJ_NAME/5_Coverages/
	SAMPLES_COV=	
	for i in mock1 mock2 mock3 ; do 
		echo $i; 
		A="$COV/coverage.$i" ; 
		SAMPLES_COV="$A $SAMPLES_COV"; 
	done
	cd $wd

Run the "join.rev.py" script to merge the output::

	$wd/join.rev2.py $GENE_PRED/id_gene2tax.$PRJ_NAME.tab $GENE_PRED/id_gene2seed.$PRJ_NAME.tab $COV/coverage.mock1 $COV/coverage.mock2 $COV/coverage.mock3 $OUT/join_gene_id_seed.tab







