Assembly
========

**Use IDBA_UD to make a co-assembly of your reads**

Source the configuration file::

	source conf_cluster.conf

First, create a directory for saving assembly files and concatenate R1 and R2 reads into two files::

	mkdir -p $ASSEMB_READS
	cat $FILT_READS/filtered*R1*fastq > $ASSEMB_READS/cat.filtered.$PRJ_NAME.R1.fastq
	cat $FILT_READS/filtered*R2*fastq > $ASSEMB_READS/cat.filtered.$PRJ_NAME.R2.fastq

Now, prepare the input files for IDBA_UD that requires interleaved FastA format::

	fq2fa --merge $ASSEMB_READS/cat.filtered.$PRJ_NAME.R1.fastq $ASSEMB_READS/cat.filtered.$PRJ_NAME.R2.fastq $ASSEMB_READS/interleaved.cat.filtered.$PRJ_NAME.fasta

See assembly parameters::

	echo "MIN_KMER_LEN= " $MIN_KMER_LEN
	echo "MAX_KMER_LEN= " $MAX_KMER_LEN
	echo "STEP= " $STEP

Run IDBA_UD::

	idba_ud -r $ASSEMB_READS/interleaved.cat.filtered.$PRJ_NAME.fasta \
	-o $ASSEMB_READS/d.$PRJ_NAME.idba \
	--mink $MIN_KMER_LEN \
	--maxk $MAX_KMER_LEN \
	--step $STEP \
	--pre_correction
	

Have a look to the obtained files::

	cd $ASSEMB_READS/d.$PRJ_NAME.idba
	ll


