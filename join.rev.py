#!/marconi_scratch/userexternal/afranzet/test_icme/icme9/env/bin/python

"""
This script 

Usage:

join.py [arg1] [arg2] [arg3] [cov1] [cov2] ... [covn] [output]

arg1: tabular file with ID gene and NCBI TAX ID
arg2: tabular file with ID gene and SEED protein
arg3: tabular file "rev.subsys.txt"
covx: tabular files from mapping with gene ID and gene coverage
output: output file name

"""
import io
import sys
import string
import pandas as pd
import numpy as np

if len(sys.argv) == 1:
	print (__doc__)
	exit(0)

print ("*Importing tax id, names and seed")

gene2tax=pd.read_table(sys.argv[1],sep='\t', index_col=0, header=None,names=["genes","tax_id"])
gene2seed=pd.read_table(sys.argv[2],sep='\t', index_col=0, header=None,names=["genes", "SEED_Level4"])

#print(gene2tax)
#print(gene2seed)

print("*Merging data tax_id and seed codes")

gene2tax_seed=pd.merge(gene2tax,gene2seed,how="outer",left_index=True,right_index=True)
gene2tax_seed.reset_index(level=0, inplace=True)

print("*Loading the seed subsystem \n")
seed_sub=pd.read_table(sys.argv[3],sep='\t')

gene2tax_seed_path=pd.merge(gene2tax_seed, seed_sub, on='SEED_Level4', how='left')

print("*Loading and merging each coverage file \n")
print(sys.argv[4])
for sample in sys.argv[4:-1]:        
        print("Considering",sample)
        print(sample.split('/')[-1])
        head=(sample.split('/')[-1])
        cov=pd.read_table(sample,sep=' ',header=None,names=["genes",head])
        #print("====>",cov)
        gene2tax_seed_path=pd.merge(gene2tax_seed_path,cov,on='genes',how='right')

gene2tax_seed_path['tax_id']= gene2tax_seed_path['tax_id'].fillna(0.0).astype(int)
print (gene2tax_seed_path)
#print(gene2tax_seed)
gene2tax_seed_path.to_csv(sys.argv[-1],sep="\t",index=False)


print("\n*Merging all together \n")

pivot=pd.pivot_table(gene2tax_seed_path, index=['tax_id','SEED_Level1','SEED_Level2','SEED_Level3','SEED_Level4'], aggfunc=np.sum)

pivot.to_csv(sys.argv[-1]+'.pivot',sep="\t")


#=============================================================
