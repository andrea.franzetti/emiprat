#!/usr/bin/python

"""
Usage:

mining.py [arg1] [arg2] [arg3]

arg1: final table 
arg2: SEED level: lev1, lev2, lev3, lev4
arg3: SEED category

"""

    
import io
import sys
import string
import pandas as pd
import numpy as np


if len(sys.argv) == 1:
	print (__doc__)
	exit(0)



grep=pd.read_table(sys.argv[1])
if sys.argv[2]=='lev1':
	grep[grep.SEED_Level1==sys.argv[3]].to_csv(sys.argv[3]+'.seed_tax.tab', index=False, sep='\t')
	pd.pivot_table(grep[grep.SEED_Level1==sys.argv[3]], index='SEED_Level1', aggfunc=np.sum).to_csv(sys.argv[3]+'.seed_tax.pivot.tab', sep='\t')
elif sys.argv[2]=='lev2':
	grep[grep.SEED_Level2==sys.argv[3]].to_csv(sys.argv[3]+'.seed_tax.tab', index=False, sep='\t')
elif sys.argv[2]=='lev3':
	grep[grep.SEED_Level3==sys.argv[3]].to_csv(sys.argv[3]+'.seed_tax.tab', index=False, sep='\t')
elif sys.argv[2]=='lev4':
	grep[grep.SEED_Level4==sys.argv[3]].to_csv(sys.argv[3]+'.seed_tax.tab', index=False, sep='\t')
else:
	print (__doc__)
	exit(0)


	

	










